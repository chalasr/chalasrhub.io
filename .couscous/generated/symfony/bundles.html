<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">

<title>Bundles Symfony</title>

<link rel="stylesheet" href="http://chalasr.github.io/css/bootstrap.min.css">
<link rel="stylesheet" href="http://chalasr.github.io/css/font-awesome.min.css">
<link rel="stylesheet" href="http://chalasr.github.io/css/highlight.dark.css">
<link rel="stylesheet" href="http://chalasr.github.io/css/main.css"><link rel="icon" href="favicon.ico">
</head>
<body>

<header class="navbar navbar-default navbar-fixed-top">

<a class="navbar-brand" href="http://chalasr.github.io/#">
Robin Chalas
<small class="hidden-xs hidden-sm">
Small blog of web developer, about web development.
</small>
</a>

<a href="https://github.com/chalasr/repositories">
<img style="position: absolute; top: 0; right: 0; border: 0;" src="https://camo.githubusercontent.com/38ef81f8aca64bb9a64448d0d70f1308ef5341ab/68747470733a2f2f73332e616d617a6f6e6177732e636f6d2f6769746875622f726962626f6e732f666f726b6d655f72696768745f6461726b626c75655f3132313632312e706e67" alt="Fork me on GitHub" data-canonical-src="https://s3.amazonaws.com/github/ribbons/forkme_right_darkblue_121621.png">
</a>

</header>

<main class="container-fluid">
<div class="row">


<nav id="sidebar" class="col-sm-3 col-lg-2" role="navigation">

<ul class="nav nav-pills nav-stacked">
<li class="">
  <a href="http://chalasr.github.io">
  Home
  </a>
</li>
<li class="">
<a href="http://chalasr.github.io/#/about">
About web
</a>
</li>
<li class="">
  <a href="http://chalasr.github.io/#/books">
    Books
  </a>
</li>
<li class="">
<a href="http://chalasr.github.io/#/works">
Works
</a>
</li>
</ul>

</nav>

<section id="content" style="width: 56%;" class="col-sm-offset-3 col-lg-offset-2 col-sm-9 col-lg-10">

<h1 id="bundle-structure-and-best-practices">Bundle Structure and Best Practices</h1>

<p>A bundle is a directory that has a well-defined structure and can host anything
from classes to controllers and web resources. Even if bundles are very
flexible, you should follow some best practices if you want to distribute them.</p>

<h2 id="bundle-name">Bundle Name</h2>

<p>A bundle is also a PHP namespace. The namespace must follow the technical
interoperability <code>standards</code>_ for PHP 5.3 namespaces and class names: it
starts with a vendor segment, followed by zero or more category segments, and
it ends with the namespace short name, which must end with a <code>Bundle</code>
suffix.</p>

<p>A namespace becomes a bundle as soon as you add a bundle class to it. The
bundle class name must follow these simple rules:</p>

<ul>
<li>Use only alphanumeric characters and underscores;</li>
<li>Use a CamelCased name;</li>
<li>Use a descriptive and short name (no more than 2 words);</li>
<li>Prefix the name with the concatenation of the vendor (and optionally the
  category namespaces);</li>
  <li>Suffix the name with <code>Bundle</code>.</li>
  </ul>

  <p>Here are some valid bundle namespaces and class names:</p>

<p>
   <br>
   Namespace                          Bundle Class Name         <br>
   <br>
   ``Acme\Bundle\BlogBundle``         ``AcmeBlogBundle``        <br>
   <br>
   ``Acme\Bundle\Social\BlogBundle``  ``AcmeSocialBlogBundle``  <br>
   <br>
   ``Acme\BlogBundle``                ``AcmeBlogBundle``        <br>
   <br>
</p>
  <p>By convention, the <code>getName()</code> method of the bundle class should return the
  class name.</p>

  <p>.. note::</p>

  <pre><code>If you share your bundle publicly, you must use the bundle class name as
  the name of the repository (``AcmeBlogBundle`` and not ``BlogBundle``
    for instance).
    </code></pre>

    <p>.. note::</p>

    <pre><code>Symfony2 core Bundles do not prefix the Bundle class with ``Symfony``
    and always add a ``Bundle`` subnamespace; for example:
    :class:`Symfony\\Bundle\\FrameworkBundle\\FrameworkBundle`.
    </code></pre>

    <p>Each bundle has an alias, which is the lower-cased short version of the bundle
    name using underscores (<code>acme_hello</code> for <code>AcmeHelloBundle</code>, or
      <code>acme_social_blog</code> for <code>Acme\Social\BlogBundle</code> for instance). This alias
      is used to enforce uniqueness within a bundle (see below for some usage
        examples).</p>

        <h2 id="directory-structure">Directory Structure</h2>

        <p>The basic directory structure of a <code>HelloBundle</code> bundle must read as
        follows:</p>

        <p>.. code-block:: text</p>

        <pre><code>XXX/...
        HelloBundle/
        HelloBundle.php
        Controller/
        Resources/
        meta/
        LICENSE
        config/
        doc/
        index.rst
        translations/
        views/
        public/
        Tests/
        </code></pre>

        <p>The <code>XXX</code> directory(ies) reflects the namespace structure of the bundle.</p>

        <p>The following files are mandatory:</p>

        <ul>
        <li><code>HelloBundle.php</code>;</li>
        <li><code>Resources/meta/LICENSE</code>: The full license for the code;</li>
        <li><code>Resources/doc/index.rst</code>: The root file for the Bundle documentation.</li>
        </ul>

        <p>.. note::</p>

        <pre><code>These conventions ensure that automated tools can rely on this default
        structure to work.
        </code></pre>

        <p>The depth of sub-directories should be kept to the minimal for most used
        classes and files (2 levels at a maximum). More levels can be defined for
        non-strategic, less-used files.</p>

        <p>The bundle directory is read-only. If you need to write temporary files, store
        them under the <code>cache/</code> or <code>log/</code> directory of the host application. Tools
        can generate files in the bundle directory structure, but only if the generated
        files are going to be part of the repository.</p>

        <p>The following classes and files have specific emplacements:</p>

        <p>
         Type                          Directory           <br>
         Commands                      <code>Command/</code>    <br>

         Controllers                   <code>Controller/</code> <br>

         Service Container Extensions  <code>DependencyInjection/</code>    <br>

         Event Listeners               <code>EventListener/</code>          <br>

         Configuration                 <code>Resources/config/</code>       <br>

         Web Resources                 <code>Resources/public/</code>       <br>

         Translation files             <code>Resources/translations/</code> <br>

         Templates                     <code>Resources/views/</code>        <br>

         Unit and Functional Tests     <code>Tests/</code>                  <br>
        </p>

        <h2 id="classes">Classes</h2>

        <p>The bundle directory structure is used as the namespace hierarchy. For
        instance, a <code>HelloController</code> controller is stored in
        <code>Bundle/HelloBundle/Controller/HelloController.php</code> and the fully qualified
        class name is <code>Bundle\HelloBundle\Controller\HelloController</code>.</p>

        <p>All classes and files must follow the Symfony2 coding :doc:<code>standards
        &lt;/contributing/code/standards&gt;</code>.</p>

        <p>Some classes should be seen as facades and should be as short as possible, like
        Commands, Helpers, Listeners, and Controllers.</p>

        <p>Classes that connect to the Event Dispatcher should be suffixed with
        <code>Listener</code>.</p>

        <p>Exceptions classes should be stored in an <code>Exception</code> sub-namespace.</p>

        <h2 id="vendors">Vendors</h2>

        <p>A bundle must not embed third-party PHP libraries. It should rely on the
        standard Symfony2 autoloading instead.</p>

        <p>A bundle should not embed third-party libraries written in JavaScript, CSS, or
        any other language.</p>

        <h2 id="tests">Tests</h2>

        <p>A bundle should come with a test suite written with PHPUnit and stored under
        the <code>Tests/</code> directory. Tests should follow the following principles:</p>

        <ul>
        <li>The test suite must be executable with a simple <code>phpunit</code> command run from
        a sample application;</li>
        <li>The functional tests should only be used to test the response output and
        some profiling information if you have some;</li>
        <li>The code coverage should at least covers 95% of the code base.</li>
        </ul>

        <p>.. note::
        A test suite must not contain <code>AllTests.php</code> scripts, but must rely on the
        existence of a <code>phpunit.xml.dist</code> file.</p>

        <h2 id="documentation">Documentation</h2>

        <p>All classes and functions must come with full PHPDoc.</p>

        <p>Extensive documentation should also be provided in the :doc:<code>reStructuredText
        &lt;/contributing/documentation/format&gt;</code> format, under the <code>Resources/doc/</code>
        directory; the <code>Resources/doc/index.rst</code> file is the only mandatory file and
        must be the entry point for the documentation.</p>

        <h2 id="controllers">Controllers</h2>

        <p>As a best practice, controllers in a bundle that’s meant to be distributed
        to others must not extend the
        :class:<code>Symfony\\Bundle\\FrameworkBundle\\Controller\\Controller</code> base class.
        They can implement
        :class:<code>Symfony\\Component\\DependencyInjection\\ContainerAwareInterface</code> or
        extend :class:<code>Symfony\\Component\\DependencyInjection\\ContainerAware</code>
        instead.</p>

        <p>.. note::</p>

        <pre><code>If you have a look at
        :class:`Symfony\\Bundle\\FrameworkBundle\\Controller\\Controller` methods,
        you will see that they are only nice shortcuts to ease the learning curve.
        </code></pre>

        <h2 id="routing">Routing</h2>

        <p>If the bundle provides routes, they must be prefixed with the bundle alias.
        For an AcmeBlogBundle for instance, all routes must be prefixed with
        <code>acme_blog_</code>.</p>

        <h2 id="templates">Templates</h2>

        <p>If a bundle provides templates, they must use Twig. A bundle must not provide
        a main layout, except if it provides a full working application.</p>

        <h2 id="translation-files">Translation Files</h2>

        <p>If a bundle provides message translations, they must be defined in the XLIFF
        format; the domain should be named after the bundle name (<code>bundle.hello</code>).</p>

        <p>A bundle must not override existing messages from another bundle.</p>

        <h2 id="configuration">Configuration</h2>

        <p>To provide more flexibility, a bundle can provide configurable settings by
        using the Symfony2 built-in mechanisms.</p>

        <p>For simple configuration settings, rely on the default <code>parameters</code> entry of
        the Symfony2 configuration. Symfony2 parameters are simple key/value pairs; a
        value being any valid PHP value. Each parameter name should start with the
        bundle alias, though this is just a best-practice suggestion. The rest of the
        parameter name will use a period (<code>.</code>) to separate different parts (e.g.
          <code>acme_hello.email.from</code>).</p>

          <p>The end user can provide values in any configuration file:</p>

          <p>.. configuration-block::</p>

          <pre><code>.. code-block:: yaml

          # app/config/config.yml
          parameters:
          acme_hello.email.from: fabien@example.com

          .. code-block:: xml

          &lt;!-- app/config/config.xml --&gt;
          &lt;parameters&gt;
          &lt;parameter key="acme_hello.email.from"&gt;fabien@example.com&lt;/parameter&gt;
          &lt;/parameters&gt;

          .. code-block:: php

          // app/config/config.php
          $container-&gt;setParameter('acme_hello.email.from', 'fabien@example.com');

          .. code-block:: ini

          [parameters]
          acme_hello.email.from = fabien@example.com
          </code></pre>

          <p>Retrieve the configuration parameters in your code from the container::</p>

          <pre><code>$container-&gt;getParameter('acme_hello.email.from');
          </code></pre>

          <p>Even if this mechanism is simple enough, you are highly encouraged to use the
          semantic configuration described in the cookbook.</p>

          <p>.. note::</p>

          <pre><code>If you are defining services, they should also be prefixed with the bundle
          alias.
          </code></pre>

          <h2 id="learn-more-from-the-cookbook">Learn more from the Cookbook</h2>

          <ul>
          <li>:doc:<code>/cookbook/bundles/extension</code></li>
          </ul>

          <p>.. _standards: https://github.com/php-fig/fig-standards/blob/master/accepted/PSR-0.md</p>
          </section>

          </div>
          </main>

          <footer>
          <div class="container-fluid">
          <p class="text-muted">
          website generated with <a href="http://couscous.io" title="Markdown website generator">Couscous</a>
          </p>
          </div>
          </footer>

          <script src="//code.jquery.com/jquery-1.11.0.min.js"></script>
          <script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
          <script src="http://yastatic.net/highlightjs/8.2/highlight.min.js"></script>

          <script>
          $(function() {
            $("section>h1").wrap('<div class="page-header" />');
            // Syntax highlighting
            hljs.initHighlightingOnLoad();
            });
            </script>

            </body>
            </html>
