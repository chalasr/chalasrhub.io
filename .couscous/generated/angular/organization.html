<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <title>AngularJS Artchitecture Organisation - Best practices</title>

  <link rel="stylesheet" href="http://chalasr.github.io/css/bootstrap.min.css">
  <link rel="stylesheet" href="http://chalasr.github.io/css/font-awesome.min.css">
  <link rel="stylesheet" href="http://chalasr.github.io/css/highlight.dark.css">
  <link rel="stylesheet" href="http://chalasr.github.io/css/main.css"><link rel="icon" href="favicon.ico">
</head>
<body>
<style>#content{width:56%;}</style>
  <header class="navbar navbar-default navbar-fixed-top">

    <a class="navbar-brand" href="http://chalasr.github.io/#">
      Robin Chalas
      <small class="hidden-xs hidden-sm">
        Small blog of web developer, about web development.
      </small>
    </a>

    <a href="https://github.com/chalasr/repositories">
      <img style="position: absolute; top: 0; right: 0; border: 0;" src="https://camo.githubusercontent.com/38ef81f8aca64bb9a64448d0d70f1308ef5341ab/68747470733a2f2f73332e616d617a6f6e6177732e636f6d2f6769746875622f726962626f6e732f666f726b6d655f72696768745f6461726b626c75655f3132313632312e706e67" alt="Fork me on GitHub" data-canonical-src="https://s3.amazonaws.com/github/ribbons/forkme_right_darkblue_121621.png">
    </a>

  </header>

  <main class="container-fluid">
    <div class="row">


      <nav id="sidebar" class="col-sm-3 col-lg-2" role="navigation">

        <ul class="nav nav-pills nav-stacked">
          <li class="">
            <a href="http://chalasr.github.io/#">
              Home
            </a>
          </li>
          <li class="">
            <a href="http://chalasr.github.io/#/about">
              About web
            </a>
          </li>
          <li class="">
            <a href="http://chalasr.github.io/#/books">
              Books
            </a>
          </li>
          <li class="">
            <a href="http://chalasr.github.io/#/works">
              Works
            </a>
          </li>
        </ul>

      </nav>

      <section id="content" class="col-sm-offset-3 col-lg-offset-2 col-sm-9 col-lg-10">
        <h1 class="post-title">Code Organization in Large AngularJS and JavaScript Applications</h1>
        <p>Many developers struggle with how to organize an application’s code base
        once it grows in size. I’ve seen this recently in AngularJS and JavaScript
        applications but historically it’s been a problem across all technologies
        including many Java and Flex apps I’ve worked on in the past.</p>

        <p>The general trend is an obsession with organizing things by type. It bears
        a striking resemblance to the way people organize their clothing.</p>

        <p>Piles on the Floor</p>

        <p>Let’s take a look at angular-seed, the official starting point for
        AngularJS apps. The "app" directory contains the following structure:</p>

        <pre><code>* css/
        * img/
        * js/
              o app.js
              o controllers.js
              o directives.js
              o filters.js
              o services.js
        * lib/
        * partials/
        </code></pre>

        <p>The JavaScript directory has one file for every type of object we write.
        This is much like organizing your clothes into different piles on the
        floor. You have a pile of socks, underwear, shirts, pants, etc. You know
        your black wool socks are in that pile in the corner but it’s going to take
        a while to dig them out.</p>

        <p>This is a mess. People shouldn’t live like this and developers shouldn’t
        code like this. Once you get beyond a half-dozen or so controllers or
        services these files become unwieldy: objects you’re looking for are hard
        to find, file changesets in source control become opaque, etc.</p>

        <p>The Sock Drawer</p>

        <p>The next logical pass at organizing JavaScript involves creating a
        directory for some of the archetypes and splitting objects into their own
        files. To continue the clothing metaphor, we’ve now invested in a nice
        mohaghony dresser and plan to put socks in one drawer, underwear in
        another, and neatly fold our pants and shirts in still others.</p>

        <p>Let’s imagine we’re building a simple e-commerce site with a login flow,
        product catalog and shopping cart UI’s. We’ve also defined new archetypes
        for Models (business logic and state) and Services (proxies to HTTP/JSON
        endpoints) rather than lumping them into Angular’s single "service"
        archetype. Our JavaScript directory can now look like this:</p>

        <pre><code>* controllers/
              o LoginController.js
              o RegistrationController.js
              o ProductDetailController.js
              o SearchResultsController.js
        * directives.js
        * filters.js
        * models/
              o CartModel.js
              o ProductModel.js
              o SearchResultsModel.js
              o UserModel.js
        * services/
              o CartService.js
              o UserService.js
              o ProductService.js
        </code></pre>

        <p>Nice! Objects can now be located easily by browsing the file tree or using
        IDE shortcuts, changesets in source control now clearly indicate what was
        modified, etc. This is a major improvement but still suffers from some
        limitations.</p>

        <p>Imagine you’re at the office and realize you need a few outfits dry-cleaned
        for a business trip tomorrow morning. You call home and ask your
        significant other to take your black charcoal and blue pinstripe suits to
        the cleaners. And don’t forget the grey shirt with the black paisley tie
        and the white shirt with the solid yellow tie. Imagine that your
        significant other is completely unfamiliar with the your dresser and
        wardrobe. As they sift through your tie drawer they see three yellow ties.
        Which one to pick?</p>

        <p>Wouldn’t it be nice if your clothing was organized by outfit? While there
        are practical constraints like cost and space that make this difficult with
        clothing in the real world, something similar can be done with code at zero
        cost.</p>

        <p>Modularity</p>

        <p>Hopefully the trite metaphors haven’t been too tedious but here’s the
        recap:</p>

        <pre><code>* Your significant other is the new developer on the team who's been
          asked to fix a bug on one of the many screens in your app.
        * The developer sifts through the directory structure and sees all the
          controllers, models and services neatly organized. Unfortunately it
          tells him/her nothing about which objects are related or have
          dependencies on one another.
        * If at some point the developer wants to reuse some of the code, they
          need to collect files from a bunch of different folders and will
          invariably forget code from another folder somewhere else.
        </code></pre>

        <p>Believe it or not, you rarely have a need to reuse all of the controllers
        from the e-commerce app in the new reporting app you’re building. You may
        however have a need to reuse some of the authentication logic. Wouldn’t it
        be nice if that was all in one place? Let’s reorganize the app based on
        functional areas:</p>

        <pre><code>* cart/
              o CartModel.js
              o CartService.js
        * common/
              o directives.js
              o filters.js
        * product/
              o search/
                    + SearchResultsController.js
                    + SearchResultsModel.js
              o ProductDetailController.js
              o ProductModel.js
              o ProductService.js
        * user/
              o LoginController.js
              o RegistrationController.js
              o UserModel.js
              o UserService.js
        </code></pre>

        <p>Any random developer can now open the top-level folder and immediately gain
        insight into what the application does. Objects in the same folder have a
        relationship and some will have dependencies on others. Understanding how
        the login and registration process work is as easy as browsing the files in
        that folder. Primitive reuse via copy/paste can at least be accomplished by
        copying the folder into another project.</p>

        <p>With AngularJS we can take this a step further and create a module of this
        related code:</p>

        <p>If we then place UserModule.js into the user folder it becomes a "manifest"
        of the objects used in that module. This would also be a reasonable place
        to add some loader directives for RequireJS or Browserify.</p>

        <p>Tips for Common Code</p>

        <p>Every application has common code that is used by many modules. We just
        need a place for it which can be a folder named "common" or "shared" or
        whatever you like. In really big applications there tends to be a lot of
        overlap of functionality and cross-cutting concerns. This can be made
        manageable through a few techniques:</p>

        <ol>
          <li>If your module’s objects require direct access to several "common"
        objects, write one or more Facades for them. This can help reduce the
        number of collaborators for each object since having too many
        collaborators is typically a code smell.</li>
          <li>If your "common" module becomes large subdivide it into submodules
        that address a particular functional area or concern. Ensure your
        application modules use only the "common" modules they need. This is
        a variant of the "Interface segregation principle" from SOLID.</li>
          <li>Add utility methods onto $rootScope so they can be used by child
        scopes. This can help prevent having to wire the same dependency
        (such as "PermissionsModel") into every controller in the
        application. Note that this should be done sparingly to avoid
        cluttering up the global scope and making dependencies non-obvious.</li>
          <li>Use events to decouple two components that don’t require an explicit
        reference to one another. AngularJS makes this possible via the
        $emit, $broadcast and $on methods on the Scope object. A controller
        can fire an event to perform some action and then receive a
        notification that the action completed.
        Quick Note on Assets and Tests</li>
        </ol>

        <p>I think there’s more room for flexibility with respect to organizing HTML,
        CSS and images. Placing them in an "assets" subfolder of the module
        probably strikes the best balance between encapsulating the module’s asset
        dependencies and not cluttering things up too much. However I think a
        separate top-level folder for this content which contains a folder
        structure that mirrors the app’s package structure is reasonable too. I
        think it works well for tests as well.”/&gt;
        <meta itemprop="name" content="Code Organization in Large AngularJS and JavaScript Applications" />
        <meta itemprop="url" content="http://cliffmeyers.com/blog/2013/4/21/code-organization-angularjs-javascript" />
        &lt;meta itemprop=”description” content=”Many developers struggle with how to organize an application’s code base
        once it grows in size. I’ve seen this recently in AngularJS and JavaScript
        applications but historically it’s been a problem across all technologies
        including many Java and Flex apps I’ve worked on in the past.</p>

        <p>The general trend is an obsession with organizing things by type. It bears
        a striking resemblance to the way people organize their clothing.</p>

        <p>Piles on the Floor</p>

        <p>Let’s take a look at angular-seed, the official starting point for
        AngularJS apps. The "app" directory contains the following structure:</p>

        <pre><code>* css/
        * img/
        * js/
              o app.js
              o controllers.js
              o directives.js
              o filters.js
              o services.js
        * lib/
        * partials/
        </code></pre>

        <p>The JavaScript directory has one file for every type of object we write.
        This is much like organizing your clothes into different piles on the
        floor. You have a pile of socks, underwear, shirts, pants, etc. You know
        your black wool socks are in that pile in the corner but it’s going to take
        a while to dig them out.</p>

        <p>This is a mess. People shouldn’t live like this and developers shouldn’t
        code like this. Once you get beyond a half-dozen or so controllers or
        services these files become unwieldy: objects you’re looking for are hard
        to find, file changesets in source control become opaque, etc.</p>

        <p>The Sock Drawer</p>

        <p>The next logical pass at organizing JavaScript involves creating a
        directory for some of the archetypes and splitting objects into their own
        files. To continue the clothing metaphor, we’ve now invested in a nice
        mohaghony dresser and plan to put socks in one drawer, underwear in
        another, and neatly fold our pants and shirts in still others.</p>

        <p>Let’s imagine we’re building a simple e-commerce site with a login flow,
        product catalog and shopping cart UI’s. We’ve also defined new archetypes
        for Models (business logic and state) and Services (proxies to HTTP/JSON
        endpoints) rather than lumping them into Angular’s single "service"
        archetype. Our JavaScript directory can now look like this:</p>

        <pre><code>* controllers/
              o LoginController.js
              o RegistrationController.js
              o ProductDetailController.js
              o SearchResultsController.js
        * directives.js
        * filters.js
        * models/
              o CartModel.js
              o ProductModel.js
              o SearchResultsModel.js
              o UserModel.js
        * services/
              o CartService.js
              o UserService.js
              o ProductService.js
        </code></pre>

        <p>Nice! Objects can now be located easily by browsing the file tree or using
        IDE shortcuts, changesets in source control now clearly indicate what was
        modified, etc. This is a major improvement but still suffers from some
        limitations.</p>

        <p>Imagine you’re at the office and realize you need a few outfits dry-cleaned
        for a business trip tomorrow morning. You call home and ask your
        significant other to take your black charcoal and blue pinstripe suits to
        the cleaners. And don’t forget the grey shirt with the black paisley tie
        and the white shirt with the solid yellow tie. Imagine that your
        significant other is completely unfamiliar with the your dresser and
        wardrobe. As they sift through your tie drawer they see three yellow ties.
        Which one to pick?</p>

        <p>Wouldn’t it be nice if your clothing was organized by outfit? While there
        are practical constraints like cost and space that make this difficult with
        clothing in the real world, something similar can be done with code at zero
        cost.</p>

        <p>Modularity</p>

        <p>Hopefully the trite metaphors haven’t been too tedious but here’s the
        recap:</p>

        <pre><code>* Your significant other is the new developer on the team who's been
          asked to fix a bug on one of the many screens in your app.
        * The developer sifts through the directory structure and sees all the
          controllers, models and services neatly organized. Unfortunately it
          tells him/her nothing about which objects are related or have
          dependencies on one another.
        * If at some point the developer wants to reuse some of the code, they
          need to collect files from a bunch of different folders and will
          invariably forget code from another folder somewhere else.
        </code></pre>

        <p>Believe it or not, you rarely have a need to reuse all of the controllers
        from the e-commerce app in the new reporting app you’re building. You may
        however have a need to reuse some of the authentication logic. Wouldn’t it
        be nice if that was all in one place? Let’s reorganize the app based on
        functional areas:</p>

        <pre><code>* cart/
              o CartModel.js
              o CartService.js
        * common/
              o directives.js
              o filters.js
        * product/
              o search/
                    + SearchResultsController.js
                    + SearchResultsModel.js
              o ProductDetailController.js
              o ProductModel.js
              o ProductService.js
        * user/
              o LoginController.js
              o RegistrationController.js
              o UserModel.js
              o UserService.js
        </code></pre>

        <p>Any random developer can now open the top-level folder and immediately gain
        insight into what the application does. Objects in the same folder have a
        relationship and some will have dependencies on others. Understanding how
        the login and registration process work is as easy as browsing the files in
        that folder. Primitive reuse via copy/paste can at least be accomplished by
        copying the folder into another project.</p>

        <p>With AngularJS we can take this a step further and create a module of this
        related code:</p>

        <p>If we then place UserModule.js into the user folder it becomes a "manifest"
        of the objects used in that module. This would also be a reasonable place
        to add some loader directives for RequireJS or Browserify.</p>

        <p>Tips for Common Code</p>

        <p>Every application has common code that is used by many modules. We just
        need a place for it which can be a folder named "common" or "shared" or
        whatever you like. In really big applications there tends to be a lot of
        overlap of functionality and cross-cutting concerns. This can be made
        manageable through a few techniques:</p>

        <ol>
          <li>If your module’s objects require direct access to several "common"
        objects, write one or more Facades for them. This can help reduce the
        number of collaborators for each object since having too many
        collaborators is typically a code smell.</li>
          <li>If your "common" module becomes large subdivide it into submodules
        that address a particular functional area or concern. Ensure your
        application modules use only the "common" modules they need. This is
        a variant of the "Interface segregation principle" from SOLID.</li>
          <li>Add utility methods onto $rootScope so they can be used by child
        scopes. This can help prevent having to wire the same dependency
        (such as "PermissionsModel") into every controller in the
        application. Note that this should be done sparingly to avoid
        cluttering up the global scope and making dependencies non-obvious.</li>
          <li>Use events to decouple two components that don’t require an explicit
        reference to one another. AngularJS makes this possible via the
        $emit, $broadcast and $on methods on the Scope object. A controller
        can fire an event to perform some action and then receive a
        notification that the action completed.
        Quick Note on Assets and Tests</li>
        </ol>

        <p>I think there’s more room for flexibility with respect to organizing HTML,
        CSS and images. Placing them in an "assets" subfolder of the module
        probably strikes the best balance between encapsulating the module’s asset
        dependencies and not cluttering things up too much. However I think a
        separate top-level folder for this content which contains a folder
        structure that mirrors the app’s package structure is reasonable too. I
        think it works well for tests as well.”/&gt;
        <link rel="author" href="https://plus.google.com/116522540335417503947/posts" />
        <meta name="twitter:title" content="Code Organization in Large AngularJS and JavaScript Applications" />
        <meta name="twitter:url" content="http://cliffmeyers.com/blog/2013/4/21/code-organization-angularjs-javascript" />
        <meta name="twitter:card" content="summary" />
        &lt;meta name=”twitter:description” content=”Many developers struggle with how to organize an application’s code base
        once it grows in size. I’ve seen this recently in AngularJS and JavaScript
        applications but historically it’s been a problem across all technologies
        including many Java and Flex apps I’ve worked on in the past.</p>

        <p>The general trend is an obsession with organizing things by type. It bears
        a striking resemblance to the way people organize their clothing.</p>

        <p>Piles on the Floor</p>

        <p>Let’s take a look at angular-seed, the official starting point for
        AngularJS apps. The "app" directory contains the following structure:</p>

        <pre><code>* css/
        * img/
        * js/
              o app.js
              o controllers.js
              o directives.js
              o filters.js
              o services.js
        * lib/
        * partials/
        </code></pre>

        <p>The JavaScript directory has one file for every type of object we write.
        This is much like organizing your clothes into different piles on the
        floor. You have a pile of socks, underwear, shirts, pants, etc. You know
        your black wool socks are in that pile in the corner but it’s going to take
        a while to dig them out.</p>

        <p>This is a mess. People shouldn’t live like this and developers shouldn’t
        code like this. Once you get beyond a half-dozen or so controllers or
        services these files become unwieldy: objects you’re looking for are hard
        to find, file changesets in source control become opaque, etc.</p>

        <p>The Sock Drawer</p>

        <p>The next logical pass at organizing JavaScript involves creating a
        directory for some of the archetypes and splitting objects into their own
        files. To continue the clothing metaphor, we’ve now invested in a nice
        mohaghony dresser and plan to put socks in one drawer, underwear in
        another, and neatly fold our pants and shirts in still others.</p>

        <p>Let’s imagine we’re building a simple e-commerce site with a login flow,
        product catalog and shopping cart UI’s. We’ve also defined new archetypes
        for Models (business logic and state) and Services (proxies to HTTP/JSON
        endpoints) rather than lumping them into Angular’s single "service"
        archetype. Our JavaScript directory can now look like this:</p>

        <pre><code>* controllers/
              o LoginController.js
              o RegistrationController.js
              o ProductDetailController.js
              o SearchResultsController.js
        * directives.js
        * filters.js
        * models/
              o CartModel.js
              o ProductModel.js
              o SearchResultsModel.js
              o UserModel.js
        * services/
              o CartService.js
              o UserService.js
              o ProductService.js
        </code></pre>

        <p>Nice! Objects can now be located easily by browsing the file tree or using
        IDE shortcuts, changesets in source control now clearly indicate what was
        modified, etc. This is a major improvement but still suffers from some
        limitations.</p>

        <p>Imagine you’re at the office and realize you need a few outfits dry-cleaned
        for a business trip tomorrow morning. You call home and ask your
        significant other to take your black charcoal and blue pinstripe suits to
        the cleaners. And don’t forget the grey shirt with the black paisley tie
        and the white shirt with the solid yellow tie. Imagine that your
        significant other is completely unfamiliar with the your dresser and
        wardrobe. As they sift through your tie drawer they see three yellow ties.
        Which one to pick?</p>

        <p>Wouldn’t it be nice if your clothing was organized by outfit? While there
        are practical constraints like cost and space that make this difficult with
        clothing in the real world, something similar can be done with code at zero
        cost.</p>

        <p>Modularity</p>

        <p>Hopefully the trite metaphors haven’t been too tedious but here’s the
        recap:</p>

        <pre><code>* Your significant other is the new developer on the team who's been
          asked to fix a bug on one of the many screens in your app.
        * The developer sifts through the directory structure and sees all the
          controllers, models and services neatly organized. Unfortunately it
          tells him/her nothing about which objects are related or have
          dependencies on one another.
        * If at some point the developer wants to reuse some of the code, they
          need to collect files from a bunch of different folders and will
          invariably forget code from another folder somewhere else.
        </code></pre>

        <p>Believe it or not, you rarely have a need to reuse all of the controllers
        from the e-commerce app in the new reporting app you’re building. You may
        however have a need to reuse some of the authentication logic. Wouldn’t it
        be nice if that was all in one place? Let’s reorganize the app based on
        functional areas:</p>

        <pre><code>* cart/
              o CartModel.js
              o CartService.js
        * common/
              o directives.js
              o filters.js
        * product/
              o search/
                    + SearchResultsController.js
                    + SearchResultsModel.js
              o ProductDetailController.js
              o ProductModel.js
              o ProductService.js
        * user/
              o LoginController.js
              o RegistrationController.js
              o UserModel.js
              o UserService.js
        </code></pre>

        <p>Any random developer can now open the top-level folder and immediately gain
        insight into what the application does. Objects in the same folder have a
        relationship and some will have dependencies on others. Understanding how
        the login and registration process work is as easy as browsing the files in
        that folder. Primitive reuse via copy/paste can at least be accomplished by
        copying the folder into another project.</p>

        <p>With AngularJS we can take this a step further and create a module of this
        related code:</p>

        <p>If we then place UserModule.js into the user folder it becomes a "manifest"
        of the objects used in that module. This would also be a reasonable place
        to add some loader directives for RequireJS or Browserify.</p>

        <p>Tips for Common Code</p>

        <p>Every application has common code that is used by many modules. We just
        need a place for it which can be a folder named "common" or "shared" or
        whatever you like. In really big applications there tends to be a lot of
        overlap of functionality and cross-cutting concerns. This can be made
        manageable through a few techniques:</p>

        <ol>
          <li>If your module’s objects require direct access to several "common"
        objects, write one or more Facades for them. This can help reduce the
        number of collaborators for each object since having too many
        collaborators is typically a code smell.</li>
          <li>If your "common" module becomes large subdivide it into submodules
        that address a particular functional area or concern. Ensure your
        application modules use only the "common" modules they need. This is
        a variant of the "Interface segregation principle" from SOLID.</li>
          <li>Add utility methods onto $rootScope so they can be used by child
        scopes. This can help prevent having to wire the same dependency
        (such as "PermissionsModel") into every controller in the
        application. Note that this should be done sparingly to avoid
        cluttering up the global scope and making dependencies non-obvious.</li>
          <li>Use events to decouple two components that don’t require an explicit
        reference to one another. AngularJS makes this possible via the
        $emit, $broadcast and $on methods on the Scope object. A controller
        can fire an event to perform some action and then receive a
        notification that the action completed.
        Quick Note on Assets and Tests</li>
        </ol>

        <p>I think there’s more room for flexibility with respect to organizing HTML,
        CSS and images. Placing them in an "assets" subfolder of the module
        probably strikes the best balance between encapsulating the module’s asset
        dependencies and not cluttering things up too much. However I think a
        separate top-level folder for this content which contains a folder
        structure that mirrors the app’s package structure is reasonable too. I
        think it works well for tests as well.”</p>
      </section>
    </div>
  </main>

  <footer>
    <div class="container-fluid">
      <p class="text-muted">
        website generated with <a href="http://couscous.io" title="Markdown website generator">Couscous</a>
      </p>
    </div>
  </footer>

  <script src="//code.jquery.com/jquery-1.11.0.min.js"></script>
  <script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
  <script src="http://yastatic.net/highlightjs/8.2/highlight.min.js"></script>

  <script>
    $(function() {
      $("section>h1").wrap('<div class="page-header" />');
      // Syntax highlighting
      hljs.initHighlightingOnLoad();
    });
  </script>

</body>
</html>
