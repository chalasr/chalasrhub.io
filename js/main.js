var projectApp = angular.module('projectApp', ['projectService','ngRoute']);


projectApp.config(function($routeProvider, $locationProvider){
		$routeProvider.when('/', {
				templateUrl:"http://chalasr.github.io/partials/home.html",
				controller:"MainCtrl"
		})
		.when('/about', {
				templateUrl:"http://chalasr.github.io/partials/about.html",
				controller:"MainCtrl"
		})
		.when('/books', {
				templateUrl:"http://chalasr.github.io/partials/books.html",
				controller:"MainCtrl"
		})
		.when('/works', {
				templateUrl:"http://chalasr.github.io/partials/projects.html",
				controller:"MainCtrl"
		})
		.otherwise({
				redirectTo:'/'
		});
});

angular.module('projectService', [])
	.factory('Project', function($http) {

		return {
			get: function() {
				return $http.get('https://api.github.com/users/chalasr/repos');
			},
			getStars: function(repo) {
				return $http.get('https://api.github.com/repos/chalasr/'+repo+'/stargazers')
			}
		}

	});

projectApp.controller('MainCtrl', function($scope, $http, Project) {
	$scope.projects = {};

		Project.get()
		.success(function(data){
			$scope.projects = data;
			$scope.count = data.length;
		});

    $scope.countStars = function(repo){
			Project.getStars(repo)
				.success(function(data){
					$scope.stars = data.length;
				})
				.error(function(){
					console.log(data);
				});
		};
});
